package org.pedroarmas.controller;

import java.net.URL;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import org.pedroarmas.bean.Productora;
import org.pedroarmas.db.Conexion;
import org.pedroarmas.sistema.Principal;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.scene.control.TableView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javax.swing.JOptionPane;
/**
 *
 * @author armas
 */
public class ProductoraController implements Initializable{     
    private enum operaciones {NUEVO, GUARDAR, ELIMINAR, EDITAR, ACTUALIZAR, CANCELAR, NINGUNO};
    private Principal escenarioPrincipal;
    private ObservableList<Productora> listaProductora;
    private operaciones tipoDeOperacion = operaciones.NINGUNO;
    @FXML private Button btnNuevo;
    @FXML private Button btnEliminar;
    @FXML private Button btnEditar;
    @FXML private Button btnReporte;    
    @FXML private TextField txtNombre;
    @FXML private ComboBox cmbProductora;
    @FXML private TableView tblProductoras;
    @FXML private TableColumn colIDProductora;
    @FXML private TableColumn colNombreProductora;    
    @Override
        public void initialize(URL location, ResourceBundle resources) {
     cargarDatos();
        } 

    public void cargarDatos(){
        tblProductoras.setItems(getProductoras());
        colIDProductora.setCellValueFactory(new PropertyValueFactory<Productora, Integer>("idProductora"));
        colNombreProductora.setCellValueFactory(new PropertyValueFactory<Productora, String>("nombre"));
    }
    
    public ObservableList<Productora> getProductoras(){
        ArrayList<Productora> lista = new ArrayList<Productora>();
        try {
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_ListarProductoras}");
            ResultSet resultado = procedimiento.executeQuery();
            while (resultado.next()) {
                lista.add(new Productora(resultado.getShort("idProductora"), resultado.getString("nombre")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listaProductora = FXCollections.observableList(lista);
    }
    
        public void reporte(){
        switch(tipoDeOperacion){
            case ACTUALIZAR: 
                btnEditar.setText("Editar");
                btnReporte.setText("Reporte");
                btnNuevo.setDisable(false);
                btnEliminar.setDisable(false);
                desactivarControles();
                limpiarControles();
                tipoDeOperacion = operaciones.NINGUNO;
            
        }
    }
    
    
    public void seleccionarElemento(){
        cmbProductora.getSelectionModel().select(buscarProductora(((Productora)tblProductoras.getSelectionModel().getSelectedItem()).getIdProductora()));
        txtNombre.setText(((Productora)tblProductoras.getSelectionModel().getSelectedItem()).getNombre());
    }
    
    public Productora buscarProductora(short idProductora){
        Productora resultado = null;
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_Buscar_Productora(?)}");
            procedimiento.setShort(1, idProductora); 
            ResultSet registro = procedimiento.executeQuery();
        while(registro.next()){
                            resultado = new Productora(registro.getShort("idProductora"),registro.getString("nombre"));
            }
        }catch(SQLException e){
            e.printStackTrace();
        }
        
        return resultado;
    }
   
    public void agregar(){
        Productora registro = new Productora();
        registro.setNombre(txtNombre.getText());
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_InsertarProductora(?)}");
            procedimiento.setString(1, registro.getNombre());
            procedimiento.execute();
            listaProductora.add(registro);
        }catch(SQLException e){
            e.printStackTrace();
        }
        
    }
    
    public void nuevo(){
        switch(tipoDeOperacion){
            case NINGUNO:
                activarControles();
                limpiarControles();
                btnNuevo.setText("Guardar");
                btnEliminar.setText("Cancelar");
                btnEditar.setDisable(true);
                btnReporte.setDisable(true);
                tipoDeOperacion = operaciones.GUARDAR;
                break;
            case GUARDAR:
                agregar();
                limpiarControles();
                btnNuevo.setText("Nuevo");
                btnEliminar.setText("Eliminar");
                btnEditar.setDisable(false);
                btnReporte.setDisable(false);
                tipoDeOperacion = operaciones.NINGUNO;
                cargarDatos();
                break;
                
        }
    }
    public void eliminar(){
        switch(tipoDeOperacion){
            case GUARDAR:
                desactivarControles();
                limpiarControles();
                btnNuevo.setText("Nuevo");
                btnEliminar.setText("Eliminar");
                btnEditar.setDisable(false);
                btnReporte.setDisable(false);
                tipoDeOperacion = operaciones.NINGUNO;
                break;
        default:
                if(tblProductoras.getSelectionModel().getSelectedItem() != null){
                    int respuesta = JOptionPane.showConfirmDialog(null, "Est� seguro de eliminar el registro?", "ELIMINAR PRODUCTORA",JOptionPane.YES_NO_OPTION,JOptionPane.QUESTION_MESSAGE);
                    if(respuesta == JOptionPane.YES_OPTION){
                        try{
                            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_ELiminarProductora(?)}");
                            procedimiento.setInt(1,((Productora) tblProductoras.getSelectionModel().getSelectedItem()).getIdProductora());
                            procedimiento.execute();
                            listaProductora.remove(tblProductoras.getSelectionModel().getSelectedIndex());
                            limpiarControles();
                            cargarDatos();
                        }catch(SQLException e){
                            e.printStackTrace();
                        }
                    }
                }else{
                    JOptionPane.showMessageDialog(null, "Debe de seleciionar una Productora");
                    }
       }
    }
 public void editar(){
        switch(tipoDeOperacion){
            case NINGUNO:
                if(tblProductoras.getSelectionModel().getSelectedItem() != null){
                    btnEditar.setText("Actualizar");
                    btnReporte.setText("Cancelar");
                    tipoDeOperacion = operaciones.ACTUALIZAR;
                    btnNuevo.setDisable(true);
                    btnEliminar.setDisable(true);
                    txtNombre.setEditable(true);
                }else{
                    JOptionPane.showMessageDialog(null, "Debe seleccionar una Categoria");
                }
            break;
            case ACTUALIZAR:
                actualizar();
                btnEditar.setText("Editar");
                btnReporte.setText("Reporte");
                btnNuevo.setDisable(false);
                btnEliminar.setDisable(false);
                limpiarControles();
                desactivarControles();
                cargarDatos();
                tipoDeOperacion = operaciones.NINGUNO;
                break;
               
        }
    }
    public void actualizar(){
        try{
            PreparedStatement procedimiento = Conexion.getInstancia().getConexion().prepareCall("{call sp_ActualizarProductora (?,?)}");
                Productora registro = (Productora)tblProductoras.getSelectionModel().getSelectedItem();
            registro.setNombre(txtNombre.getText());
            registro.setIdProductora(((Productora)cmbProductora.getSelectionModel().getSelectedItem()).getIdProductora());
            procedimiento.setInt(1, registro.getIdProductora());
            procedimiento.setString(2, registro.getNombre());
            procedimiento.execute();
        }catch(SQLException e){
            e.printStackTrace();
        }
    }
    
    public void desactivarControles(){
        txtNombre.setEditable(false);
        cmbProductora.setDisable(true);
    }
    
    public void activarControles(){
        txtNombre.setEditable(true);
        cmbProductora.setDisable(true);
    }
    
    public void limpiarControles(){
        txtNombre.setText("");
        cmbProductora.setValue("");
    }

 
    
    
  

    public Principal getEscenarioPrincipal() {
        return escenarioPrincipal;
    }

    public void setEscenarioPrincipal(Principal escenarioPrincipal) {
        this.escenarioPrincipal = escenarioPrincipal;
    }
    
    public void menuPrincipal(){
        escenarioPrincipal.menuPrincipal();
    }
    
}